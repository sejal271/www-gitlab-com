---
layout: markdown_page
title: "Category Direction - Build Artifacts"
description: "Usage and administration of Build Artifacts"
canonical_path: "/direction/verify/build_artifacts/"
---

- TOC
{:toc}

## Build Artifacts

[Artifacts](https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts) are files created as part of a build process that often contain metadata about that build's jobs like test results, security scans, etc. These can be used for reports that are displayed directly in GitLab or can be published to GitLab pages or in some other way for users to review. These artifacts can provide a wealth of knowledge to development teams and the users they support.

[Job Artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) and [Pipeline Artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html) are both included in the scope of Build Artifacts to empower GitLab CI users to more effectively manage testing capabilities across their software lifecycle in both the gitlab-ci.yml or as the latest output of a job.

For information about storing containers or packages or information about release evidence see the [Package Stage direction page](https://about.gitlab.com/handbook/product/categories/#package-stage).

## Additional Resources

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20artifacts)
- [Overall Vision of the Verify stage](/direction/ops/#verify)

For specific information and features related to display of artifact data, check out the [GitLab Features](/features/) and for information about administration of artifacts please reference the Job Artifact [documentation](https://docs.gitlab.com/ee/administration/job_artifacts.html). You may also be looking for one of the related direction pages from the [Verify Stage](/direction/ops/#verify-stage-categories).

## What's Next & Why

Address [performance problems](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20artifacts&label_name[]=performance) related to writing, finding, reading, and deleting build artifacts that create a bad user experience in GitLab.
