---
layout: markdown_page
title: "Category Strategy - Backup and Restore"
description: "GitLab supports backup and restore procedures that rely
on standard unix tools."
canonical_path: "/direction/geo/backup_restore/"
---

- TOC
{:toc}

## 🗄 Backup and Restore

**Last updated**: 2020-12-23

### Introduction and how you can help

- [Overall Strategy](/direction/geo/)
- No roadmap is available yet.
- [Maturity: <%= data.categories["backup_restore"].maturity.capitalize %>](/direction/maturity/)
- [Documentation](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
- [Complete Maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/2299)
- [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances)

Please reach out to Nick Nguyen, Acting Product Manager for the Geo group
([Email](mailto:nnguyen@gitlab.com)) if you'd like to provide feedback or ask
any questions related to this product category.

This strategy is a work in progress, and everyone can contribute. Please comment and contribute in the linked
[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

⚠️ The Geo group is focusing on improving GitLab's [Disaster Recovery
capabilities](https://about.gitlab.com/direction/geo/disaster_recovery/). We do support backup and restore on a best-effort basis. This
means, we fix bugs in line with our SLOs but we have no capacity to contribute major feature improvements, such as incremental backups. We will re-evaluate the priority of this work in Q1 FY22.
{: .alert .alert-warning}

### Overview

GitLab supports [backup and restore
procedures](https://docs.gitlab.com/ee/raketasks/backup_restore.html) that rely
on standard unix tools, such as `rsync` and `tar`. By default, backups cover
most data but not GitLab’s configuration. For GitLab instances that contain
several hundred gigabytes or even terabytes, the current solution does not scale
well. This means that backing up or restoring such a GitLab instance can take many hours, which is a major issue.

### Why is this important?

GitLab is a crucial tool for many customers and if GitLab does not handle
backups, customers will be forced to implement their own strategies. This is not
efficient and leads to a very heterogeneous landscape that is difficult to
maintain and support. GitLab should offer backup and restore capabilities for
any scale and offer clear guidance for all reference architectures. This is
important because because it enables customers to fit GitLab into their business
continuity plans.

### Target audience and experience

Backup and restore tools are primarily used by [Sidney - (Systems
Administrator)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator).

Backups should be complete, easy to create, automate, and restore. They also need
to complete as fast as possible, for example, to support point in time recovery.

### What's Next & Why

In Q1 FY22 we will re-evaluate the priority of the backup and restore category and are going to assess [all currently open
issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances)
to define the priorities further.

#### GitLab backups should scale well

GitLab is used by customers with thousands of users and terabytes of data.
[Backups should be fast at any
scale](https://gitlab.com/gitlab-org/gitlab/-/issues/28780). This means that
GitLab should not only support base backups but also [incremental
backups](https://gitlab.com/gitlab-org/gitlab/-/issues/19256). Copying terabytes
of data every time when a backup is performed is not efficient at this scale and
can take many hours. Backups should also because be agnostic with regards to the
backend - local storage, cloud storage etc. should all be configurable.

Currently, GitLab uses `rsync` to create backups and we should investigate
alternatives e.g [restic](https://restic.net/) to see if those help address some
of these concerns.

### In one year

Backups should be incremental and work better at scale. We should be able to
utilise a Geo secondary for backup related tasks.

#### Restoring specific data should be easy

Sometimes, a user may remove a single project by accident. In those cases, it
may be desirable to restore [only individual items from the
backup](https://gitlab.com/gitlab-org/gitlab/-/issues/14084). This should
ideally be possible via the UI and can be performed by a systems administrator.

#### Backing up from a Geo instance

Sometimes a GitLab primary instance is under pressure from heavy usage and
backing up may add additional load that is not desirable. As Geo becomes more
complete, it will contain most if not all data from a primary. This means,
[backups should be able to run on a
secondary](https://gitlab.com/gitlab-org/gitlab/-/issues/211668), thereby
reducing the pressure on the secondary. This is especially desirable for the
PostgreSQL database.

### In three years

Backup plans should be part of all reference architectures and should be enabled
by default. When using such a reference architecture, a GitLab Backup should be able to recreate the entire architecture, configuration and restore all data.

Systems administrators should be able to use a complete UI and CLI that gives
them access to all backup related functions.

#### Backup and Restore via the UI

Currently, all backup tasks are performed via `rake` tasks in the command line.
This is not necessarily a problem but it can result [in low
visibility](https://gitlab.com/gitlab-org/gitlab/-/issues/13965) and requires
systems administrators to switch between different user interfaces. There is no
place in the GitLab user interface that allows systems administrators to access
backups.

GitLab should offer a dedicated Backup and Restore section that allows some of
these functionalities:

- listing recent backups and their status
- scheduling backups
- configuring backup tasks
- restoring from a backup
- etc.

### What is not planned right now

We are in the process of defining the product direction and are not in a
position to answer this yet.

### Maturity plan

This category is currently at the <%= data.categories["backup_restore"].maturity
%> maturity level, and our next maturity target is complete (see our
[definitions of maturity levels](/direction/maturity/)).

We are still investigating what is required to move the category from  <%=
data.categories["backup_restore"].maturity %> to complete. You can track the
work in the [viable maturity
epic](https://gitlab.com/groups/gitlab-org/-/epics/2299).

### User success metrics

- Time to backup / time to restore
- Number of manual steps required to be performed by a systems administrator to
  backup and restore.
- Number of backup/restore operations performed via Web UI

### Competitive landscape

All major competitors offer backup solutions for their products.

### Analyst landscape

We do need to interact more closely with analysts to understand the landscape
better.

### Top customer success/sales issue(s)

- [Most popular backup and restore issues with customer label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances&label_name[]=customer)

### Top user issues

- [Most popular backup and restore issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ABackup%2FRestore%20of%20GitLab%20instances)

### Top internal customer issues/epics

Not yet available.

### Top strategy item(s)

- [Backups should be performant at any scale](https://gitlab.com/gitlab-org/gitlab/-/issues/28780)
- [Backups should support incremental backups](https://gitlab.com/gitlab-org/gitlab/-/issues/19256)
- [Backups should be a part of the administrator UI](https://gitlab.com/gitlab-org/gitlab/-/issues/13965)
